function sum(input){

    if (toString.call(input) !== "[object Array]")
	return false;

    var total =  0;
    for(var i=0;i<input.length;i++)
    {
        if(isNaN(input[i])){
            continue;
        }
        total += Number(input[i]);
    }
    return total;
}



custom_self_paced_reading_stimulus = function(config, CT) {
    return `<div class='magpie-view'>
               <h1 class='magpie-view-title'>${config.title}</h1>
               <p class='magpie-view-question magpie-view-qud'>${config.data[CT].QUD}</p>
               <div class='magpie-view-stimulus-container'>
                   <div class='magpie-view-stimulus magpie-nodisplay'>
                   </div>
               </div>
               <table class='magpie-spr-sentence'>
               </table>
            </div>
            <div>
               <div class='magpie-view-answer-container magpie-nodisplay'>
               <p class='magpie-view-question'>${config.data[CT].question}</p>
                    <span class='magpie-response-slider-option'>Completely Disagree</span>
                    <input type='range' id='response' class='magpie-response-slider' min='0' max='100' value='50'/>
                    <span class='magpie-response-slider-option'>Completely Agree</span>
                </div>
            <button id='next' class='magpie-view-button'>Next</button>`;
}

custom_self_paced_reading = function(config, CT, magpie, answer_container_generator, startingTime){
    let statement_list = config.data[CT].args
    const list_length = config.data[CT].number
    let readingTimes = [];
    let response_list;
    let block_list;
    // shows the statements one at a time on click
    let counter;
    counter = 0;
    const handle_next_click = function() {
        // shows the next sentence
        if (counter < list_length) {
            block_list[counter].classList.remove("spr-word-hidden");
            counter++
        } else if (counter === list_length) {
            $(".magpie-view-answer-container").removeClass("magpie-nodisplay");
	    $(".magpie-view-button").addClass("magpie-invisible");
            counter++
        } else {
            counter = 0
            const RT = Date.now() - startingTime; // measure RT before anything else
	    let response = document.getElementById('response').value
            let trial_data = {
		trial_name: config.name,
		trial_number: CT + 1,
		response: response,
            };
            trial_data = magpieUtils.view.save_config_trial_data(config.data[CT], trial_data);
            magpie.trial_data.push(trial_data);
	magpie.findNextView();
	}
    }
    // creates the list
    statement_list.map((statement, index) => {
        $(".magpie-spr-sentence").append(
            `<tr>
               <td class='spr-word spr-word-hidden'>
                 ${statement}
               </td>
             </tr>`
        );
    });



    // creates an array of spr word elements
    block_list = $(".spr-word").toArray();
    response_list = $(".magpie-response-slider").toArray();
    block_list[0].classList.remove("spr-word-hidden");
    counter++

// attaches a listener on the next button
    $("#next").on("click", handle_next_click);

    // ensure that all sliders are moved before showing next button
    $("#response").on("click", function() {$("#next").removeClass("magpie-invisible")})
    $("#response").on("change", function() {$("#next").removeClass("magpie-invisible")})

}
