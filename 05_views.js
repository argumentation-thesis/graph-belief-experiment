// In this file you can instantiate your views
// We here first instantiate wrapping views, then the trial views


/* Wrapping views below

* Obligatory properties

    * trials: int - the number of trials this view will appear
    * name: string

*Optional properties
    * buttonText: string - the text on the button (default: 'next')
    * text: string - the text to be displayed in this view
    * title: string - the title of this view

    * More about the properties and functions of the wrapping views - https://magpie-project.github.io/magpie-docs/01_designing_experiments/01_template_views/#wrapping-views

*/

const intro = magpieViews.view_generator("intro", {
  trials: 1,
  name: 'intro',
  // If you use JavaScripts Template String `I am a Template String`, you can use HTML <></> and javascript ${} inside
  text: `Thank you for your participation in our study!
         Your anonymous data makes an important contribution to our understanding of human language use.
          <br />
          <br />
          Legal information:
          By answering the following questions, you are participating in a study
          being performed by scientists from the University of Osnabrueck, Germany.
          <br />
          <br />
          You must be at least 18 years old to participate.
          <br />
          <br />
          Your participation in this research is voluntary.
          You may decline to answer any or all of the following questions.
          You may decline further participation, at any time, without adverse consequences.
          <br />
          <br />
          Your anonymity is assured; the researchers who have requested your
          participation will not receive any personal information about you.
          `,
  buttonText: 'Begin the experiment'
});


// For most tasks, you need instructions views
const general_instructions = magpieViews.view_generator("instructions",{
    trials: 1,
    name: 'general_instrucions',
    title: 'General Instructions',
    text:  `This is a survey about social events. We would like your opinion about different social events and reasons for liking them.
            The survey should take 5 to 10 minutes. There are no right answers, only your opinions are needed.
           `,
    buttonText: 'Next'
});



// For most tasks, you need instructions views
const weighting_instructions = magpieViews.view_generator("instructions",{
    trials: 1,
    name: 'weighting_instructions',
    title: 'Task 1 Instructions',
    text:  `Your first task is to rate your level of agreemeent/disagreement with different statements.
            Try to consider each statement on its own without comparing it to others.
            <br />
            <br />
            A slider will be shown below each statement. Move the slider to rate how much you agree or disagree with the statement. Once you have moved
            the slider to your desired position, click 'Next' to continue to the next
            statement.
           `,
    buttonText: 'begin task 1'
});


// For most tasks, you need instructions views
const acceptability_instructions = magpieViews.view_generator("instructions",{
    trials: 1,
    name: 'acceptability_instructions',
    title: 'Task 2 Instructions',
    text:  `For the second task, suppose that you and a group of friends are trying to decide on an event. A few arguments have been made about each possible event.
            The statements will appear one after another. You will then be asked to rate how much you agree or disagree with the conclusion by moving a slider.
           `,
    buttonText: 'begin task 2'
});




// In the post test questionnaire you can ask your participants addtional questions
const post_test = magpieViews.view_generator("post_test",{
    trials: 1,
    name: 'post_test',
    title: 'Additional information',
    text: 'Answering the following questions is optional, but your answers will help us analyze our results.',
    edu_higher_degree: "",

    // You can change much of what appears here, e.g., to present it in a different language, as follows:
    // buttonText: 'Weiter',
    // age_question: 'Alter',
    // gender_question: 'Geschlecht',
    // gender_male: 'männlich',
    // gender_female: 'weiblich',
    // gender_other: 'divers',
    // edu_question: 'Höchster Bildungsabschluss',
    // edu_graduated_high_school: 'Abitur',
    // edu_graduated_college: 'Hochschulabschluss',
    // edu_higher_degree: 'Universitärer Abschluss',
    // languages_question: 'Muttersprache',
    // languages_more: '(in der Regel die Sprache, die Sie als Kind zu Hause gesprochen haben)',
    // comments_question: 'Weitere Kommentare'
});

// The 'thanks' view is crucial; never delete it; it submits the results!
const thanks = magpieViews.view_generator("thanks", {
    trials: 1,
    name: 'thanks',
    title: 'Thank you for taking part in this experiment!',
    prolificConfirmText: 'Press the button'
});

/** trial (magpie's Trial Type Views) below

* Obligatory properties

    - trials: int - the number of trials this view will appear
    - name: string - the name of the view type as it shall be known to _magpie (e.g. for use with a progress bar)
            and the name of the trial as you want it to appear in the submitted data
    - data: array - an array of trial objects

* Optional properties

    - pause: number (in ms) - blank screen before the fixation point or stimulus show
    - fix_duration: number (in ms) - blank screen with fixation point in the middle
    - stim_duration: number (in ms) - for how long to have the stimulus on the screen
      More about trial life cycle - https://magpie-project.github.io/magpie-docs/01_designing_experiments/04_lifecycles_hooks/

    - hook: object - option to hook and add custom functions to the view
      More about hooks - https://magpie-project.github.io/magpie-docs/01_designing_experiments/04_lifecycles_hooks/

* All about the properties of trial views
* https://magpie-project.github.io/magpie-docs/01_designing_experiments/01_template_views/#trial-views
*/


// Here, we initialize a normal forced_choice view
const weighting_trials = magpieViews.view_generator("slider_rating", {
    // This will use all trials specified in `data`, you can user a smaller value (for testing), but not a larger value
    trials: trial_info.weighting_trials.length,
    // name should be identical to the variable name
    name: 'weighting_trials',
    data: _.shuffle(trial_info.weighting_trials),
    // you can add custom functions at different stages through a view's life cycle
    // hook: {
    //     after_response_enabled: check_response
    // }
});

// There are many more templates available:
// forced_choice, slider_rating, dropdown_choice, testbox_input, rating_scale, image_selection, sentence_choice,
// key_press, self_paced_reading and self_paced_reading_rating_scale


// const acceptability_trials = magpieViews.view_generator("self_paced_reading_rating_scale", {
//     trials: trial_info.acceptability_trials.length,
//     name: "acceptability_trials",
//     data: trial_info.acceptability_trials,
// });

const acceptability_trials = magpieViews.view_generator("self_paced_reading",{
    trials: trial_info.acceptability_trials.length,
    name: "acceptability_trials",
    data: _.shuffle(trial_info.acceptability_trials)
}, {
    stimulus_container_generator: custom_self_paced_reading_stimulus,
    handle_response_function: custom_self_paced_reading
});
